resource "azurerm_container_registry" "shared" {
  name                = "acriacworkshopshared${var.randomstring}" # only alpha numeric characters allowed, add random stirng for uniqueness
  resource_group_name = azurerm_resource_group.shared.name
  location            = azurerm_resource_group.shared.location
  sku                 = "Basic"
  admin_enabled       = true
}
