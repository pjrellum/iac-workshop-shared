resource "azurerm_resource_group" "shared" {
  name     = "rg-iacworkshop-shared"
  location = var.location

  tags = local.tags
}
